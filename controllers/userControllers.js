const User = require("../models/User");

const Product = require("../models/Product");
//encrypted password
const bcrypt = require('bcrypt');

const auth = require("../auth");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		//if match is found
		if(result.length > 0){
			return true;
		} else {
			//No duplicate email found
			//The user is not yet registered in the database
			return false;
		}
	})
};



//User Registration
module.exports.registerUser = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return 'User already Exist';
		} else {

	//Creates a new User Object
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//Saves the created object to our database
	return newUser.save().then((user, error) => {
		//User registration failed
		if(error) {
			return false;
		} else {
			//User Registration is success
			return true
		}

		})
	
		}
	})
}



//User Authentication (User Login)
module.exports.loginUser = (reqBody) => {
	//findOne it will returns the first record in the collection that matches the search criteria

	return User.findOne({ email: reqBody.email }).then(result => {
		//User does not exist
		if(result == null){
			return false;
		} else {
			//User exists

			//The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false".
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			//if the password match
			if(isPasswordCorrect){
				//Generate an access token
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				//Password does not match
				return false;
			}

		};
	});
};



// =================================================
// Set User as ADMIN

module.exports.updateIsAdmin = (userId, reqBody) => {
	// specify the properties of the document to be updated.
	let updateIsAdmin = {
		
		isAdmin: reqBody.isAdmin
	};

	// .findByIdAndUpdate (id, updatesToBeApplied)
	return User.findByIdAndUpdate(userId, updateIsAdmin).then((user, error) => {

		// admin not updated.
		if(error) {
			return false;
		} else {
			// admin updated successfully
			return 'Admin Updated Successfully';
		}

	})
}


// =================================================





//Get user details
module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		
		result.password = "";
		result.orders = "";

		return result;
	})
}


//Checkout order
module.exports.createOrder = async (data) => {
	
	//Add the productId to the orders array of the user
	let isUserUpdated = await User.findById(data.userId).then( user => {

		//push the productId to orders property
		user.orders.push({productId: data.productId});

		//save
		return user.save().then((user, error) => {
			if(error) {
				return false;
			}else {
				return true;
			}
		})
	});


	let isProductUpdated = await Product.findById(data.productId).then(product => {
		//add the userId in the course's database(enrollees)
		product.customers.push({userId: data.userId});

		return product.save().then((product, error) => {
			if(error) {
				return false;
			}else {
				return true;
			}
		})
	});


	//Validation
	if(isUserUpdated && isProductUpdated){
		//user checkout successful
		return true;
	}else {
		//user checkout failed
		return false;
	}

};


//Get User Orders
module.exports.getOrder = (data) => {
	return User.findById(data).then(result => {
		
		return result.orders;
	})
};




//Get All Orders.
// module.exports.getAllOrder = () => {
// 	return User.find({}).then(result => {
// 		let allOrder = result.map(function(results){
// 			return results.order;
// 			})
		
// 		return results;
// 	})
// }


//Retrieve all orders.
module.exports.getAllOrder = () => {
	return User.find({}).then(result => {
		let allOrder = [];
		
		for(let i = 0; i < result.length; i++){
			if(result[i].orders !=0){
				allOrder.push(result[i].email, result[i].orders);
				;
			
				
			} else {}
		}
		
		return allOrder;
	})
}