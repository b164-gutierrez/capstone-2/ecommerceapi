const Product = require('../models/Product');

const auth = require('../auth');


// Create a new Product.
module.exports.addProduct = (reqBody) => {

	return Product.find({name: reqBody.product.name}).then(result => {
		if(result.length > 0){
			return 'Product already Exist';
		} else {

	//Create a new object
	let newProduct = new Product({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price
	});

			//Saves the created object to our database
			return newProduct.save().then((product, error) => {
				//Product creation failed
				if(error) {
					return false;
				} else {
					//Product Creation successful
					return 'Product Created Successfully!';
				}
			})
		}
	})

}




// Retrieve All Product.
module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		let rx = result.map(function(resultx){
			return resultx.customers = "";
		})
		
		return result;
	})
}



//Retrieve All ACTIVE Product.
module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then(result => {
		let ax = result.map(function(results){
			return results.customers = "";
			})
		
		return result;
	})
}




// Retrieve a Specific Product.
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {

		result.customers = "";
		return result;
	})
}




// Update a Product
module.exports.updateProductInfo = (productId, reqBody) => {
	// specify the properties of the document to be updated.
	let updateProductInfo = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	// .findByIdAndUpdate (id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updateProductInfo).then((product, error) => {

		// product not updated.
		if(error) {
			return false;
		} else {
			// product updated successfully
			return true;
		}

	})
}



// Update isActive/Archive Product
module.exports.updateArchive = (productId, reqBody) => {
	// specify the properties of the document to be updated.
	let updateArchive = {
		
		isActive: reqBody.isActive
	};

	// .findByIdAndUpdate (id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updateArchive).then((product, error) => {

		// product not updated.
		if(error) {
			return false;
		} else {
			// product updated successfully
			return 'Successfully Updated!';
		}

	})
}









