const express = require("express");
const router = express.Router();
const auth = require("../auth");


const UserController = require("../controllers/userControllers");


router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result));
});


// User Registration
// http://localhost:5000/api/users/register
router.post('/register', (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
})


// User Authentication (User Login)
router.post('/login', (req, res) =>{
	UserController.loginUser(req.body).then(result => res.send(result));
})


// =========================================
// Set User as Admin
// router.put('/:userId/admin', (req, res) => {
// 		UserController.updateIsAdmin(req.params.userId, req.body).then(result => res.send(result));
// 	});

router.put('/:userId/admin', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.updateIsAdmin(req.params.userId, req.body).then(result => res.send(result));
	} else {
		res.send(false);
	}
})


// Get User Details
// 'auth.verify' acts as a middleware to ensure that the user is logged in before they can get the details of a user.
router.get('/details', auth.verify, (req, res) => {

	// decode() to retrieve the user information from the token passing the 'token' from the request headers as an argument.

	const userData = auth.decode(req.headers.authorization);

	UserController.getProfile(userData.id).then(result => res.send(result));
})




// Checkout Order
router.post('/checkout', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	UserController.createOrder(data).then(result => res.send(result));

})


// Get User "Order"
// 'auth.verify' acts as a middleware to ensure that the user is logged in before they can get the details of a user.
router.get('/order', auth.verify, (req, res) => {

	// decode() to retrieve the user information from the token passing the 'token' from the request headers as an argument.

	const userData = auth.decode(req.headers.authorization);

	UserController.getOrder(userData.id).then(result => res.send(result));
})



//Get All Order (Admin)
router.get('/allOrders', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.getAllOrder(data).then(result => res.send(result));
	} else {
		res.send(false);
	}
})




module.exports = router;
