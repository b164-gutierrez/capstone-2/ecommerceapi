const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/productControllers');

const auth = require("../auth");

const User = require("../models/User");


// Creating a Product.
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		ProductController.addProduct(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})




// Get All Product.
router.get("/all", (req, res) => {
	ProductController.getAllProduct().then(result => res.send(result));
})



//Retrieving All ACTIVE Product
router.get("/", (req, res) => {
	ProductController.getAllActive().then(result => res.send(result))
});



// Retrieving a Specific Product.
router.get('/:productId', (req, res) =>{
	console.log(req.params.productId);

	ProductController.getProduct(req.params.productId).then(result => res.send(result));
})




// Update a Product
router.put('/:productId', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.updateProductInfo(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false);
	}
})



// Update Archive (true/false)
router.put('/:productId/archive', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ProductController.updateArchive(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false);
	}
})




// Enroll User to a Product
// router.post('/register', auth.verify, (req, res) => {
// 	const data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		productId : req.body.productId
// 	}
// })









module.exports = router;











