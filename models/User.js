const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile no. is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},

			transactionDate: {
				type: Date,
				default: new Date()
			},
			paymentStatus: {
				type: String,
				default: "Paid"
			}
		}

	]



})

module.exports = mongoose.model("User", userSchema)


