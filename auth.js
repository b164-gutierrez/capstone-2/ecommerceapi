const jwt = require('jsonwebtoken');
const secret = 'BeeArCapstone2';

module.exports.createAccessToken = (user) => {
	
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
}



module.exports.verify = (req, res, next) => {

		// token is retrieved from the request header.
		let token = req.headers.authorization;

		// if token is recieved and is not undefined.
		if(typeof token !== 'undefined'){

			console.log(token);
			// Bearer 2b$10$Hv0ob74EX6o. (to remove 'Bearer' string use slice method)
			token = token.slice(7, token.length);

			// Validate the token using the 'verify' method, decrypting the token using secret method.

			return jwt.verify(token, secret, (err, data)=> {
				// if JWT is not valid
				if(err) {
					return res.send({auth: 'failed'});
				} else {
					// if JWT is valid
					next()
				}
			})


		} else {

			// if token does not exist
			return res.send({ auth: 'token undefined' })
		}

}


module.exports.decode = (token) => {

	// Token recieved and is not undefined.
	if(typeof token !== 'undefined'){

		// Retrieves only the token and removes the 'Bearer' prefix.
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}	else {
				return jwt.decode(token, {complete: true}).payload; //payload = data
			}
		})

	} else {
		// token does not exist
		return null
	}

}